#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include "matrix.hpp"

int input(double* A, double* b, int input_method, int n, FILE* in)
{
    int i,j;
    if (input_method == 1)
        for (i=0; i<n; i++)
        {
            for (j=0; j<n; j++)
                if (fscanf(in,"%lf",&A[i*n+j]) != 1)
                    return -1;
            if (fscanf(in,"%lf",&b[i]) != 1)
                return -1;
        }
    else
    {
        for (i=0; i<n; i++)
        {
            b[i] = 0;
            
            for(j=0; j<n; j++)
            {
                A[i*n+j] = f(i,j,n);
                if (j % 2 == 0)
                    b[i] += A[i*n+j];
            }
        }
    }
    return 0;
}

void Display(int n, int m, double* x)
{
    int i;
    if (m > n) m = n;
    printf("\nSolution\n");
    for (i=0; i<m; i++)
        printf("%f ",x[i]);
    printf("\n\n");
    return;
}

double f(int i, int j, int n)
{
//    return(fabs(i-j));
    return i+j == n - 1 ? 1 : 0;
//    return 0;
}
