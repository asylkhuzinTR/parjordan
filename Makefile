prog: main.o matrix.o solution.o
	g++ $^ -o prog -O3
main.o: main.cpp matrix.hpp solution.hpp
	g++ -c $< -O3
matrix.o: matrix.cpp matrix.hpp solution.hpp
	g++ -c $< -O3
solution.o: solution.cpp matrix.hpp solution.hpp
	g++ -c $< -O3